# Important !!!

This project is curently being moved to: [https://gitlab.cern.ch/acc-logging-team/pytimber](https://gitlab.cern.ch/acc-logging-team/pytimber)
and it will be under responsibility of **NXCALS team**.

Please **do not** perform any commits to this repository as it may cause inconsistency during the transition period.

Please use [acc-logging-support@cern.ch](mailto:acc-logging-support@cern.ch) for the support requests or any questions that you may have.

In the future any modifications to this repository will be ignored.
